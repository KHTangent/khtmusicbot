import { Client, Intents } from "discord.js";
import { readFileSync } from "fs";
import { join as pathJoin } from "path";

import { AudioManager } from "./AudioModule";

console.log("Loading config...");
const config = JSON.parse(
	readFileSync(pathJoin(__dirname, "../config.json")).toString()
);
if (!config.token) {
	console.log("Please set a token in config.json");
	process.exit();
}
const prefix = config.prefix || "!";
console.log("Loaded config.");

const bot = new Client({
	intents: [
		Intents.FLAGS.GUILDS,
		Intents.FLAGS.GUILD_VOICE_STATES,
		Intents.FLAGS.GUILD_MESSAGES,
	],
});
const audioManager = new AudioManager();

const ytRegex =
	/(?:https?:\/\/)?(?:youtu\.be\/|(?:www\.|m\.)?youtube\.com\/(?:watch|v|embed)(?:\.php)?(?:\?.*v=|\/))([a-zA-Z0-9_-]+)/;

bot.on("ready", () => {
	console.log("Connected to Discord.");
});

bot.on("messageCreate", (msg) => {
	if (msg.content.startsWith(`${prefix}play `)) {
		const query = msg.content.substring(`${prefix}play `.length);
		if (query.length == 0) return;
		if (ytRegex.test(query)) {
			audioManager.addUrl(msg, query);
		} else {
			audioManager.addSearch(msg, query);
		}
	} else if (msg.content == `${prefix}stop`) {
		audioManager.stopPlaying(msg);
	} else if (msg.content == `${prefix}skip`) {
		audioManager.skipCurrent(msg);
	} else if (msg.content == `${prefix}now` || msg.content == `${prefix}np`) {
		audioManager.nowPlaying(msg);
	} else if (msg.content == `${prefix}help`) {
		msg.channel.send(
			"List of commands: \n" +
				`- \`${prefix}play [title/url]\`: Enqueue a track by title or URL\n` +
				`- \`${prefix}skip\`: Skip the current track\n` +
				`- \`${prefix}stop\`: Stop playing and clear the queue\n` +
				`- \`${prefix}now\`: See what's currently playing\n` +
				`- \`${prefix}np\`: alias for \`${prefix}now\``
		);
	}
});

process.on("SIGINT", async () => {
	bot.destroy();
	console.log("Exiting...");
	process.exit();
});

bot.login(config.token);
