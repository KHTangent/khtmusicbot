import ytdl = require("ytdl-core");
import YouTube, { Video } from "youtube-sr";
import { Message, TextChannel } from "discord.js";
import {
	joinVoiceChannel,
	createAudioResource,
	VoiceConnection,
	createAudioPlayer,
	AudioPlayer,
	NoSubscriberBehavior,
	AudioPlayerStatus,
	AudioResource,
	entersState,
	VoiceConnectionStatus,
	DiscordGatewayAdapterCreator,
} from "@discordjs/voice";
import { EventEmitter } from "events";

export class AudioManager {
	private activeChannels = new Map<string, ChannelPlayer>();

	async addUrl(msg: Message, url: string): Promise<void> {
		let vidInfo: ytdl.videoInfo;
		try {
			vidInfo = await ytdl.getBasicInfo(url);
		} catch (e: unknown) {
			msg.channel.send("Error getting video.");
			return;
		}
		const vcId = await this.joinChannel(msg);
		if (!vcId) return;
		this.activeChannels.get(vcId)?.add({
			title: vidInfo.videoDetails.title,
			duration: secondsToString(parseInt(vidInfo.videoDetails.lengthSeconds)),
			url: url,
		});
		if (!this.activeChannels.get(vcId)?.playing) {
			this.activeChannels.get(vcId)?.playNext();
		}
	}

	async addSearch(msg: Message, query: string): Promise<void> {
		let results;
		try {
			results = await YouTube.search(query, { limit: 5, type: "video" });
		} catch (e: unknown) {
			msg.channel.send("Error searching.");
			return;
		}
		if (results.length === 0) {
			msg.channel.send("No results found.");
			return;
		}
		const firstResult = results[0] as Video;
		if (!firstResult.title || !firstResult.id) {
			msg.channel.send("Error in video format.");
			return;
		}
		const vcId = await this.joinChannel(msg);
		if (!vcId) return;
		this.activeChannels.get(vcId)?.add({
			title: firstResult.title,
			duration: firstResult.durationFormatted,
			url: firstResult.id, // ytdl accepts both URL and id
		});
		if (!this.activeChannels.get(vcId)?.playing) {
			this.activeChannels.get(vcId)?.playNext();
		}
	}

	async nowPlaying(msg: Message): Promise<void> {
		if (!msg.member?.voice.channel) {
			msg.channel.send("Please connect to a voice channel first.");
			return;
		}
		this.activeChannels.get(msg.member.voice.channel.id)?.now();
	}

	async skipCurrent(msg: Message): Promise<void> {
		if (!msg.member?.voice.channel) {
			msg.channel.send("Please connect to a voice channel first.");
			return;
		}
		this.activeChannels.get(msg.member.voice.channel.id)?.skip();
	}

	async stopPlaying(msg: Message): Promise<void> {
		if (!msg.member?.voice.channel) {
			msg.channel.send("Please connect to a voice channel first.");
			return;
		}
		this.activeChannels.get(msg.member.voice.channel.id)?.stop();
	}

	// Joins the voice channel of a sender if not already in it. Returns the channel
	// ID if join was successful, or the bot was in the channel already
	private async joinChannel(msg: Message): Promise<string | null> {
		if (!msg.member?.voice.channel) {
			msg.channel.send("Please connect to a voice channel first.");
			return null;
		}
		const vcId = msg.member.voice.channel.id;
		if (!this.activeChannels.has(vcId)) {
			try {
				const vChannel = msg.member.voice.channel;
				const conn = joinVoiceChannel({
					channelId: vChannel.id,
					guildId: vChannel.guildId,
					adapterCreator: vChannel.guild.voiceAdapterCreator as DiscordGatewayAdapterCreator,
				});
				await entersState(conn, VoiceConnectionStatus.Ready, 5000);
				this.activeChannels.set(
					vcId,
					new ChannelPlayer(conn, msg.channel as TextChannel)
				);
				this.activeChannels.get(vcId)?.on("stopped", () => {
					this.activeChannels.delete(vcId);
				});
			} catch (e: unknown) {
				msg.channel.send("Could not connect to voice channel.");
				return null;
			}
		}
		return vcId;
	}
}

class ChannelPlayer extends EventEmitter {
	private connection: VoiceConnection;
	private player: AudioPlayer;
	private queue: Array<QueuedVideo>;
	private currentVideo?: QueuedVideo;
	private currentResource?: AudioResource;
	private statusChannel: TextChannel;
	playing = false;

	constructor(conn: VoiceConnection, statusChannel: TextChannel) {
		super();
		this.connection = conn;
		this.statusChannel = statusChannel;
		this.queue = [];
		this.player = createAudioPlayer({
			behaviors: {
				noSubscriber: NoSubscriberBehavior.Play,
			},
		});
		this.player.on("error", (err) => {
			console.error(err);
			this.statusChannel.send("Something went wrong: `" + err.message + "`");
		});
		this.connection.subscribe(this.player);
		this.player.on(AudioPlayerStatus.Idle, (oldState) => {
			if (oldState.status !== AudioPlayerStatus.Playing) {
				return;
			}
			this.statusChannel.send(
				`Finished playing** ${this.currentVideo?.title}**`
			);
			if (this.queue.length === 0) {
				this.stop();
			} else {
				this.playNext();
			}
		});
	}

	add(vidInfo: QueuedVideo) {
		this.queue.push(vidInfo);
		this.statusChannel.send(`Queued** ${vidInfo.title} **`);
	}

	playNext() {
		if (this.queue.length == 0) {
			this.statusChannel.send("Something went wrong while starting playback.");
			return;
		}
		this.currentVideo = this.queue.shift() as QueuedVideo;
		const vid = ytdl(this.currentVideo.url, {
			filter: "audioonly",
			dlChunkSize: 0,
			highWaterMark: 8000000,
		});
		this.currentResource = createAudioResource(vid);
		this.player.play(this.currentResource);
		this.statusChannel.send(`Playing** ${this.currentVideo.title}**`);
		this.playing = true;
	}

	now() {
		if (!this.playing || !this.currentVideo || !this.currentResource) {
			this.statusChannel.send("Nothing is playing right now.");
		} else {
			const currentTime = this.currentResource.playbackDuration / 1000;
			this.statusChannel.send(
				`Now playing: \n **` +
					`${this.currentVideo?.title} **` +
					`\`[${secondsToString(currentTime)}/${this.currentVideo.duration}]\``
			);
		}
	}

	skip() {
		this.player.stop();
	}

	stop() {
		this.queue = [];
		if (this.player.state.status !== AudioPlayerStatus.Idle) {
			this.player.stop();
		}
		this.playing = false;
		this.connection.disconnect();
		this.emit("stopped");
	}
}

interface QueuedVideo {
	title: string;
	duration: string;
	url: string;
}

function secondsToString(secs: number): string {
	const totalSeconds = Math.round(secs);
	const seconds = totalSeconds % 60;
	const minutes = Math.floor(totalSeconds / 60);
	return `${minutes}:${("" + seconds).padStart(2, "0")}`;
}
